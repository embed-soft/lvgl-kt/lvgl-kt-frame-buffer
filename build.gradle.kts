import java.util.Properties
import org.jetbrains.dokka.gradle.DokkaTask

val projectSettings = fetchProjectSettings()
val mavenCentralSettings = fetchMavenCentralSettings()
val secretPropsFile = File("maven_central.properties")
val homeDir = System.getenv("HOME") ?: ""
val lvglDriversDir = "$homeDir/c_libs/lv_drivers"
val lvglDir = "$homeDir/c_libs/lvgl/lvgl-8.3.3"

group = "io.gitlab.embed-soft"
version = if (projectSettings.isDevVer) "${projectSettings.libVer}-dev" else projectSettings.libVer

plugins {
    kotlin("multiplatform") version "1.7.21"
    id("org.jetbrains.dokka") version "1.7.20"
    `maven-publish`
    signing
}

ext["signing.keyId"] = mavenCentralSettings.signingKeyId
ext["signing.password"] = mavenCentralSettings.signingPassword
ext["signing.secretKeyRingFile"] = mavenCentralSettings.signingKeyRingFile
ext["ossrhUsername"] = mavenCentralSettings.ossrhUsername
ext["ossrhPassword"] = mavenCentralSettings.ossrhPassword

repositories {
    mavenCentral()
    mavenLocal()
}

val dokkaHtml by tasks.getting(DokkaTask::class)

val javadocJar by tasks.register("javadocJar", Jar::class) {
    dependsOn(dokkaHtml)
    archiveClassifier.set("javadoc")
    from(dokkaHtml.outputDirectory)
}

kotlin {
    val lvDriversVer = "8.3"
    val useEvDev = "-DUSE_EVDEV=1"
    val lvDriversCompilerOpts = arrayOf(
        "-DLV_CONF_SKIP",
        "-DLV_DRV_NO_CONF",
        "-D_REENTRANT",
        "-DLV_LVGL_H_INCLUDE_SIMPLE",
        "-DUSE_MOUSE",
        "-DUSE_MONITOR",
        "-DUSE_KEYBOARD",
        "-DUSE_MOUSEWHEEL"
    )
    explicitApi()

    linuxArm32Hfp("linuxArm32") {
        compilations.getByName("main") {
            dependencies {
                implementation("io.gitlab.embed-soft:lvglkt-core:$version")
                implementation("io.gitlab.embed-soft:lvglkt-drivers:$version")
            }

            cinterops.create("lv_drivers") {
                includeDirs(
                    lvglDir,
                    "$lvglDriversDir/lv_drivers-$lvDriversVer/display",
                    "$lvglDriversDir/lv_drivers-$lvDriversVer/indev",
                    lvglDriversDir,
                    "/usr/include",
                    "/usr/include/x86_64-linux-gnu"
                )
                compilerOpts(*lvDriversCompilerOpts, useEvDev)
            }
        }
    }

    sourceSets {
        commonMain {
            dependencies {
                val kotlinVer = "1.7.21"
                implementation(kotlin("stdlib", kotlinVer))
                implementation("io.gitlab.embed-soft:lvglkt-core:$version")
                implementation("io.gitlab.embed-soft:lvglkt-drivers:$version")
            }
        }
    }
}

publishing {
    publications.withType<MavenPublication> {
        if (projectSettings.includeDocs) artifact(javadocJar)
        createPom()
    }
    repositories {
        if (mavenCentralSettings.publishingEnabled) createMavenCentralRepo()
    }
}

val Boolean.intValue: Int
    get() = if (this) 1 else 0

signing {
    if (secretPropsFile.exists()) sign(publishing.publications)
}

fun getExtraString(name: String) = ext[name]?.toString()

fun MavenPublication.createPom() = pom {
    name.set("LVGL KT Frame Buffer")
    description.set("Provides support for LVGL's Frame Buffer display backend.")
    url.set("https://gitlab.com/embed-soft/lvgl-kt/lvgl-kt-frame-buffer")

    licenses {
        license {
            name.set("Apache 2.0")
            url.set("https://opensource.org/licenses/Apache-2.0")
        }
    }
    developers {
        developer {
            id.set("NickApperley")
            name.set("Nick Apperley")
            email.set("napperley@protonmail.com")
        }
    }
    scm {
        url.set("https://gitlab.com/embed-soft/lvgl-kt/lvgl-kt-frame-buffer")
    }
}

fun RepositoryHandler.createMavenCentralRepo() {
    maven {
        name = "sonatype"
        setUrl("https://s01.oss.sonatype.org/service/local/staging/deploy/maven2/")
        credentials {
            username = getExtraString("ossrhUsername")
            password = getExtraString("ossrhPassword")
        }
    }
}

fun fetchProjectSettings(): ProjectSettings {
    var libVer = "test"
    var isDevVer = false
    var includeDocs = false
    val properties = Properties()
    val file = File("project.properties")
    if (file.exists()) {
        file.inputStream().use { inputStream ->
            properties.load(inputStream)
            libVer = properties.getProperty("libVer") ?: "SNAPSHOT"
            @Suppress("RemoveSingleExpressionStringTemplate")
            isDevVer = "${properties.getProperty("isDevVer")}".toBoolean()
            @Suppress("RemoveSingleExpressionStringTemplate")
            includeDocs = "${properties.getProperty("includeDocs")}".toBoolean()
        }
    }
    return ProjectSettings(libVer = libVer, isDevVer = isDevVer, includeDocs = includeDocs)
}

fun fetchMavenCentralSettings(): MavenCentralSettings {
    var signingKeyId = ""
    var signingPassword = ""
    var signingKeyRingFile = "keyRing"
    var ossrhUsername = ""
    var ossrhPassword = ""
    var publishingEnabled = false
    val properties = Properties()
    val file = File("maven_central.properties")
    if (file.exists()) {
        file.inputStream().use { inputStream ->
            properties.load(inputStream)
            signingKeyId = properties.getProperty("signing.keyId")
            @Suppress("RemoveSingleExpressionStringTemplate")
            publishingEnabled = "${properties.getProperty("mavenCentral.publishingEnabled")}".toBoolean()
            signingPassword = properties.getProperty("signing.password")
            signingKeyRingFile = properties.getProperty("signing.secretKeyRingFile")
            ossrhUsername = properties.getProperty("ossrhUsername")
            ossrhPassword = properties.getProperty("ossrhPassword")
        }
    }
    return MavenCentralSettings(
        signingKeyId = signingKeyId,
        signingPassword = signingPassword,
        signingKeyRingFile = signingKeyRingFile,
        publishingEnabled = publishingEnabled,
        ossrhUsername = ossrhUsername,
        ossrhPassword = ossrhPassword
    )
}

data class MavenCentralSettings(
    val signingKeyId: String,
    val signingPassword: String,
    val signingKeyRingFile: String,
    val ossrhUsername: String,
    val ossrhPassword: String,
    val publishingEnabled: Boolean
)

data class ProjectSettings(val libVer: String, val isDevVer: Boolean, val includeDocs: Boolean)
