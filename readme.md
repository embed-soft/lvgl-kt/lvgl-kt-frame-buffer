# LVGL KT Frame Buffer (lvglkt-frame-buffer)

A Kotlin Native library that provides Kotlin bindings to the Frame Buffer backend part of the 
[LV Drivers library](https://github.com/lvgl/lv_drivers). This library depends on Kotlin Native (currently in beta), 
and [LVGL KT Drivers](https://gitlab.com/embed-soft/lvgl-kt/lvgl-kt-drivers). Below is the key functionality provided 
by this library:

- Creation and management of the frame buffer

The following Kotlin Native targets are supported:
- linuxArm32Hfp


## Requirements

The following is required by the library in order to use it:

- Kotlin Native 1.7.21 or later
- Gradle 6.9.2 or later
- LV Drivers 8.3

Note that the **liblv_drivers.a** file (a static library file) **MUST** be generated for a Kotlin Native program using
this library, from the LV Drivers source code via [cmake](https://cmake.org/). Alternatively an existing
**liblv_drivers.a** file can be acquired from the
[Counter sample](https://gitlab.com/embed-soft/lvgl-kt/samples/counter-sample/-/tree/master/lib).


## Usage

To use this library in a Kotlin Native program using Gradle add the library as a dependency. Add the following to
the build file: `implementation(io.gitlab.embed-soft:lvglkt-frame-buffer:0.4.0)`


## Frame Buffer

This library supports using the Frame Buffer as a display backend. In order to use the Frame Buffer do the following,
eg:
```kotlin
// ...
FrameBuffer.createDisplay(newHorRes = 1024.toShort(), newVertRes = 768.toShort(), enableDblBuffer = true)
```
