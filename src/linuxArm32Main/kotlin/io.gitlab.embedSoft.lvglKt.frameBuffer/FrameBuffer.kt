package io.gitlab.embedSoft.lvglKt.frameBuffer

import io.gitlab.embedSoft.lvglKt.core.Area
import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.lv_disp_flush_ready
import io.gitlab.embedSoft.lvglKt.core.display.DisplayDrawBuffer
import io.gitlab.embedSoft.lvglKt.core.display.DisplayDriver
import io.gitlab.embedSoft.lvglKt.core.styling.Color
import io.gitlab.embedSoft.lvglKt.drivers.clib.lvDrivers.fbdev_exit
import io.gitlab.embedSoft.lvglKt.drivers.clib.lvDrivers.fbdev_flush
import io.gitlab.embedSoft.lvglKt.drivers.clib.lvDrivers.fbdev_init
import io.gitlab.embedSoft.lvglKt.drivers.register
import kotlinx.cinterop.memScoped

public actual object FrameBuffer {
    public actual fun open() {
        fbdev_init()
    }

    public actual fun close() {
        fbdev_exit()
    }

    public actual fun flush(
        displayDriver: DisplayDriver,
        area: Area,
        color: Color
    ): Unit = memScoped {
        fbdev_flush(drv = displayDriver.lvDispDrvPtr, area = area.lvAreaPtr, color_p = color.lvColorPtr)
        lv_disp_flush_ready(displayDriver.lvDispDrvPtr)
    }

    public actual fun createDisplayDriver(drawBuf: DisplayDrawBuffer, init: DisplayDriver.() -> Unit): DisplayDriver {
        val result = DisplayDriver.create(drawBuf = drawBuf)
        result.onFlush = { driver, area, buf -> flush(displayDriver = driver, area = area, color = buf) }
        result.init()
        return result
    }

    public actual fun createDisplay(
        newHorRes: Short,
        newVertRes: Short,
        enableDblBuffer: Boolean
    ): Pair<DisplayDrawBuffer, DisplayDriver> {
        // A small buffer for LVGL to draw the screen's content.
        val drawBuf = DisplayDrawBuffer.create((newHorRes * newVertRes).toUInt(), enableDblBuffer)
        val driver = createDisplayDriver(drawBuf) {
            horRes = newHorRes
            vertRes = newVertRes
            register()
        }
        return drawBuf to driver
    }
}
