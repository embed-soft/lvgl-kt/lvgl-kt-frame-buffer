package io.gitlab.embedSoft.lvglKt.frameBuffer

import io.gitlab.embedSoft.lvglKt.core.Area
import io.gitlab.embedSoft.lvglKt.core.display.DisplayDrawBuffer
import io.gitlab.embedSoft.lvglKt.core.display.DisplayDriver
import io.gitlab.embedSoft.lvglKt.core.styling.Color

/** Covers all functionality with the Frame Buffer display backend. */
public expect object FrameBuffer {
    /** Initializes the Frame Buffer driver. */
    public fun open()

    /** Safely closes the Frame Buffer driver, and frees up unused resources. */
    public fun close()

    /**
     * Refreshes the Frame Buffer.
     * @param displayDriver The display driver to use.
     * @param area The area to use.
     * @param color The color to use.
     */
    public fun flush(displayDriver: DisplayDriver, area: Area, color: Color)

    /**
     * Creates the display driver that uses the Frame Buffer display backend.
     * @param drawBuf The draw buffer to use.
     * @param init The lambda that initializes the object.
     * @return The new [DisplayDriver].
     */
    public fun createDisplayDriver(drawBuf: DisplayDrawBuffer, init: DisplayDriver.() -> Unit): DisplayDriver

    /**
     * Creates a display (includes the display buffer and driver) for output via the Frame Buffer backend.
     * @param newHorRes New horizontal resolution.
     * @param newVertRes New vertical resolution.
     * @param enableDblBuffer If *true* then double buffering is used.
     * @return A Pair the contains the following:
     * 1. Display buffer
     * 2. Display driver
     */
    public fun createDisplay(
        newHorRes: Short,
        newVertRes: Short,
        enableDblBuffer: Boolean
    ): Pair<DisplayDrawBuffer, DisplayDriver>
}
